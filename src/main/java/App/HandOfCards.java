package App;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * The type Hand of cards.
 */
public class HandOfCards {
    private PlayingCard[] handOfCards;

    /**
     * Instantiates a new Hand of cards.
     *
     * @param handOfCards the hand of cards
     */
    public HandOfCards(PlayingCard[] handOfCards) {
        this.handOfCards = handOfCards;
    }

    /**
     * Check for flush string.
     *
     * @return the string
     */
    public String checkForFlush() {
        if (Arrays.stream(handOfCards).map(PlayingCard::getSuit).distinct().count()==1) {
            return "Yes";
        } else {
            return "No";
        }
    }

    /**
     * Check sum of hand int.
     *
     * @return the int
     */
    public int checkSumOfHand() {
        return Arrays.stream(handOfCards).mapToInt(PlayingCard::getFace).sum();
    }

    /**
     * Check suit of cards string.
     *
     * @return the string
     */
    public String checkSuitOfCards() {
        char suit = 'H';
        return Arrays.stream(handOfCards).filter(s-> s.getSuit() == suit).collect(Collectors.toList()).toString();
    }

    /**
     * Checks hand for a specific card.
     * @param suit
     * @param face
     * @return
     */
    public boolean checkForCard() {
        char suit = 'S';
        int face = 12;
        String cardToCheck = String.format("%s%s", suit, face);
        if (Arrays.stream(handOfCards).filter(s -> s.getAsString() == cardToCheck).count()==1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Simple toString method.
     * @return
     */
    @Override
    public String toString() {
        return  Arrays.toString(handOfCards);
    }
}
