import App.DeckOfCards;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Nested;
import static org.junit.jupiter.api.Assertions.*;

@Nested
public class DeckOfCardTest {


    @Test
    public void createADeckOfCardsTest() {
        DeckOfCards deck = new DeckOfCards();
        assertEquals(52, deck.getDeck().size());
    }

    @Test
    public void dealAHandTestDealsCorrectAmountOfCards() {
        DeckOfCards testDeck = new DeckOfCards();
        testDeck.dealHand(5);
        assertEquals(47, testDeck.getDeck().size());
    }
}
