package App;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;


/**
 * The type Card game controller.
 */
public class CardGameController {
    /**
     * The Deck of cards.
     */
    DeckOfCards deckOfCards = new DeckOfCards();


    /**
     * The N cards in deck.
     */
    @FXML
    public Label nCardsInDeck;

    /**
     * The Your hand text field.
     */
    @FXML
    public TextField yourHandTextField;

    /**
     * The Amount of cards.
     */
    @FXML
    public TextField amountOfCards;

    /**
     * The Sum of faces field.
     */
    @FXML
    public TextField sumOfFacesField;

    /**
     * The Flush field.
     */
    @FXML
    public TextField flushField;

    /**
     * The Queen field.
     */
    @FXML
    public TextField queenField;

    /**
     * The Heart field.
     */
    @FXML
    public TextField heartField;

    /**
     * Gets card hand.
     *
     * @param event the event
     */
    public void getCardHand(ActionEvent event) {
        int n = Integer.parseInt(amountOfCards.getText());
        HandOfCards yourHand = deckOfCards.dealHand(n);

        /**
         * Updates all of the cardfields
         */
        nCardsInDeck.setText(String.valueOf(deckOfCards.getDeckSize()));
        yourHandTextField.setText(String.valueOf(yourHand));
        sumOfFacesField.setText(String.valueOf(yourHand.checkSumOfHand()));
        flushField.setText(String.valueOf(yourHand.checkForFlush()));
        queenField.setText(String.valueOf(yourHand.checkForCard()));
        heartField.setText(yourHand.checkSuitOfCards());
    }

    /**
     * Reset the deck.
     *
     * @param event the event
     */
    public void resetTheDeck(ActionEvent event) {
        deckOfCards = new DeckOfCards();
        nCardsInDeck.setText(String.valueOf(deckOfCards.getDeckSize()));
    }


}
