import App.HandOfCards;
import App.PlayingCard;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Nested;
import static org.junit.jupiter.api.Assertions.*;
@Nested
public class HandOfCardsTest {


    @Test
    public void checkForFlushMethodTest() {
        PlayingCard[] testHand = new PlayingCard[5];
        testHand[0] = new PlayingCard('H', 1);
        testHand[1] = new PlayingCard('H', 2);
        testHand[2] = new PlayingCard('H', 3);
        testHand[3] = new PlayingCard('H', 4);
        testHand[4] = new PlayingCard('H', 5);

        PlayingCard[] testHandWithoutFlush = new PlayingCard[2];
        testHandWithoutFlush[0] = new PlayingCard('H', 1);
        testHandWithoutFlush[1] = new PlayingCard('S', 1);

        HandOfCards handWithFlush = new HandOfCards(testHand);
        HandOfCards handWithoutFlush = new HandOfCards(testHandWithoutFlush);

        assertEquals("Yes", handWithFlush.checkForFlush());
        assertNotEquals("Yes", handWithoutFlush.checkForFlush());
    }

    @Test
    public void checkForExpectedSumOfHandTes() {
        PlayingCard[] testHand = new PlayingCard[3];
        testHand[0] = new PlayingCard('H', 1);
        testHand[1] = new PlayingCard('H', 2);
        testHand[2] = new PlayingCard('H', 3);

        HandOfCards testHandSum = new HandOfCards(testHand);

        assertEquals(6, testHandSum.checkSumOfHand());
    }

    @Test
    public void checkForTheSuitOfCardsTest() {
        PlayingCard[] testHand = new PlayingCard[3];
        testHand[0] = new PlayingCard('H', 1);
        testHand[1] = new PlayingCard('H', 2);
        testHand[2] = new PlayingCard('H', 3);

        HandOfCards testHandHearts = new HandOfCards(testHand);
        String emptyString = "";
        assertNotEquals(emptyString, testHandHearts.checkSuitOfCards());
    }

    @Test
    public void checkForASpecificCardTest() {
        PlayingCard[] testHand = new PlayingCard[3];
        testHand[0] = new PlayingCard('H', 1);
        testHand[1] = new PlayingCard('H', 2);
        testHand[2] = new PlayingCard('S', 12);

        HandOfCards testHandWithQueenOfSpades = new HandOfCards(testHand);
        assertEquals(true, testHandWithQueenOfSpades.checkForCard());
    }

}
