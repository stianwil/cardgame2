package App;

import java.util.ArrayList;
import java.util.Random;


/**
 * The type Deck of cards.
 */
public class DeckOfCards {
    private ArrayList<PlayingCard> deck;
    private final char[] suit = { 'S', 'H', 'D', 'C'};
    private final int face = 0;
    /**
     * The Rand.
     */
    Random rand = new Random();

    /**
     * Instantiates a new Deck of cards.
     */
    public DeckOfCards() {
        this.deck = new ArrayList<PlayingCard>();
        for (char c : suit) {
            for (int j = 1; j <= 13; j++) {
                deck.add(new PlayingCard(c, j));
            }
        }
    }

    /**
     * Deal hand of cards.
     *
     * @param n the n
     * @return the hand of cards
     */
    public HandOfCards dealHand(int n) {
        PlayingCard[] cardHand = new PlayingCard[n];

        for (int i = 0; i < n; i++) {
            PlayingCard cardToAddAndRemove = deck.get(rand.nextInt(getDeckSize()));
            cardHand[i] = cardToAddAndRemove;
            deck.remove(cardToAddAndRemove);
            deck.trimToSize();
        }
        return new HandOfCards(cardHand);
    }

    /**
     * Gets deck.
     *
     * @return the deck
     */
    public ArrayList<PlayingCard> getDeck() {
        return deck;
    }

    /**
     * Gets deck size.
     *
     * @return the deck size
     */
    public int getDeckSize() {
       return deck.size();
    }
}
